package mv.controller;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.transform.Scale;
import mv.gui.Workspace;
import properties_manager.PropertiesManager;
import saf.AppTemplate;

/**
 * @author Calvin Li 109535588 
 * CSE214
 */

public class MapViewerController {

/**
 * This class responds to interactions with map viewing controls.
 *
 * @author McKillaGorilla
 * @author Calvin Li
 * @version 1.1
 */

    AppTemplate app;

    PropertiesManager props = PropertiesManager.getPropertiesManager();
    

    public MapViewerController(AppTemplate initApp) {
        app = initApp;
    }
    
    public void zoomHandler(MouseButton button, double x, double y) {
        //center
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace();   
        switch (button) {
            case PRIMARY: 
                //then zoom in 
                workspace.sc.setPivotX(x);
                workspace.sc.setPivotY(y);
                workspace.sc.setX(2);
                workspace.sc.setY(2);
                workspace.getWork().getTransforms().add(workspace.sc);
                workspace.getWork().setScaleX(workspace.getWork().getScaleX() * 2.0);
                workspace.getWork().setScaleY(workspace.getWork().getScaleY() * 2.0);
                workspace.getWork().setLayoutX(workspace.getWork().getLayoutX() + app.getGUI().getPrimaryScene().getWidth()/2-x);
                workspace.getWork().setLayoutY(workspace.getWork().getLayoutY() + app.getGUI().getPrimaryScene().getHeight()/2-y);

                break;
            case SECONDARY:
                //zoom out
                workspace.getWork().setScaleX(workspace.getWork().getScaleX() / 2.0);
                workspace.getWork().setScaleY(workspace.getWork().getScaleY() / 2.0);
                workspace.getWork().setLayoutX(workspace.getWork().getLayoutX() + app.getGUI().getPrimaryScene().getWidth()/2-x);
                workspace.getWork().setLayoutY(workspace.getWork().getLayoutY() + app.getGUI().getPrimaryScene().getHeight()/2-y);
        }
        workspace.reloadWorkspace();
    }

    public void keyPressHandler(KeyCode e) {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace();
        
        switch (e) {
            case UP: workspace.getWork().setTranslateY(workspace.getWork().getTranslateY()+20);
                break; 
            case DOWN: workspace.getWork().setTranslateY(workspace.getWork().getTranslateY()-20);
                break;
            case LEFT: workspace.getWork().setTranslateX(workspace.getWork().getTranslateX()+20);
                break;
            case RIGHT:workspace.getWork().setTranslateX(workspace.getWork().getTranslateX()-20);
                break;
            case G: workspace.drawLines(!workspace.isShowingLines);
                    workspace.isShowingLines = !workspace.isShowingLines;
                    break;
        }
        
        workspace.reloadWorkspace();
    }
    
}