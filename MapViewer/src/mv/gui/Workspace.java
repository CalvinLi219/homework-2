/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.gui;

import java.util.ArrayList;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Scale;
import saf.components.AppWorkspaceComponent;
import mv.MapViewerApp;
import mv.controller.MapViewerController;
import mv.data.DataManager;
import saf.AppTemplate;
import saf.ui.AppGUI;

/**
 *@author Calvin Li
 * @author McKillaGorilla
 */
public class Workspace extends AppWorkspaceComponent {
    
    MapViewerApp app;
    
    AppGUI gui;
    
    MapViewerController mvController = new MapViewerController(app);
    
    public boolean isShowingLines = true;
    
    ArrayList<Line> lines = new ArrayList();
    
    public Scale sc = new Scale();
    
    
    public Workspace(MapViewerApp initApp) {
        app = initApp;
        gui = app.getGUI();
        
        workspace = new Pane();
        FlowPane temp = (FlowPane)gui.getAppPane().getTop();
        temp.getChildren().remove(0);
        temp.getChildren().remove(1);
        workspace.toBack();
        setupHandlers();
    }

    @Override
    public void reloadWorkspace() {
        DataManager dataManager = (DataManager)app.getDataComponent();
        this.getWork().toBack();
    }

    @Override
    public void initStyle() {
        this.getWork().setStyle("-fx-background-color: #5094D4; -fx-text-fill: white;");
    }
    
    public Pane getWork() {
        return this.workspace;
    }
    
    private void setupHandlers() {
	// MAKE THE CONTROLLER
	mvController = new MapViewerController(app);
	
	// NOW CONNECT THE BUTTONS TO THEIR HANDLERS
        app.getGUI().getPrimaryScene().setOnKeyPressed(e->{
            mvController.keyPressHandler(e.getCode());
        });
        
        app.getGUI().getPrimaryScene().setOnMouseClicked(e->{
            mvController.zoomHandler(e.getButton(), e.getSceneX(), e.getSceneY());
        });
        
    }
    
    public void drawLines(Boolean showingLines) {
        if (showingLines) {
            DataManager dataManager = (DataManager)app.getDataComponent();
            //adding longitude lines
            for (double i = 0; i < dataManager.getWidth(); i+= dataManager.getWidth()/12) {
                Line line = new Line(20, 100, 270, 100);
                if ((i != 0) && (i != dataManager.getWidth()/2) && (i != dataManager.getWidth())) 
                    line.getStrokeDashArray().addAll(2d);
                line.setStartX(i);
                line.setStartY(0);
                line.setEndX(i);
                line.setEndY(dataManager.getHeight());
                line.setStroke(Color.WHITE);
                this.getWork().getChildren().add(line);
                lines.add(line);
            }

            //adding latitude lines
            for (double i = 0; i < dataManager.getHeight(); i+= dataManager.getHeight()/6) {
                Line line = new Line(20, 100, 270, 100);
                if (i != dataManager.getHeight()/2)
                    line.getStrokeDashArray().addAll(2d);
                line.setStartX(0);
                line.setStartY(i);
                line.setEndX(dataManager.getWidth());
                line.setEndY(i);
                line.setStroke(Color.WHITE);
                this.getWork().getChildren().add(line);
                lines.add(line);
            }
            
            //add top line
            Line line = new Line(20,100,270,100);
            line.getStrokeDashArray().addAll(2d);
            line.setStartX(0);
            line.setStartY(dataManager.getHeight());
            line.setEndX(dataManager.getWidth());
            line.setEndY(dataManager.getHeight());
            line.setStroke(Color.WHITE);
            this.getWork().getChildren().add(line);
            lines.add(line);
        } else {
            this.getWork().getChildren().removeAll(lines);
        }
    }
    
}
