package mv.data;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.transform.Scale;
import saf.components.AppDataComponent;
import mv.MapViewerApp;
import mv.gui.Workspace;
import saf.AppTemplate;

/**
 *@author Calvin Li
 * @author McKillaGorilla
 */
public class DataManager implements AppDataComponent {
    AppTemplate app;
    
    ObservableList<Polygon> polygons;
    
    public DataManager(MapViewerApp initApp) {
        app = initApp;
        polygons = FXCollections.observableArrayList();
    }
    
    @Override
    public void reset() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        polygons.clear();
        workspace.getWork().getChildren().clear();
        workspace.sc = new Scale();
    }
    
    public void addPolygon(Polygon polygon) {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        workspace.getWork().setScaleY(-1);
        polygons.add(polygon);
        workspace.getWork().getChildren().add(polygon);
        polygon.setFill(Color.OLIVEDRAB);
        polygon.setStroke(Color.BLACK);
        workspace.drawLines(true);
        workspace.reloadWorkspace();
    }
        
    public double getWidth() {
        return app.getGUI().getPrimaryScene().getWidth();
    }
    
    public double getHeight() {
        double totalHeight = app.getGUI().getWindow().getScene().getHeight();
        double toolbarHeight = app.getGUI().getAppPane().getTop().getLayoutBounds().getHeight();
        return totalHeight-toolbarHeight;
    }
    
}
