/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.file;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.shape.Polygon;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import mv.MapViewerApp;
import mv.data.DataManager;
import mv.gui.Workspace;
import saf.AppTemplate;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;

/**
 *@author Calvin Li
 * @author McKillaGorilla
 */
public class FileManager implements AppFileComponent {
    
    MapViewerApp app;
    static int ptCounter;
    double width;
    double height;
    Double[] ArrPolygonPts;

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        DataManager dataManager = (DataManager)data;
        dataManager.reset();
        
        width = dataManager.getWidth();
        height = dataManager.getHeight();
        Polygon polygon;
                        
        JsonObject json = loadJSONFile(filePath);
        JsonArray subregionsArray = json.getJsonArray("SUBREGIONS");
        for (int i = 0; i < subregionsArray.size(); i++){
            JsonObject region = subregionsArray.getJsonObject(i);
            JsonArray subregionPolygons = region.getJsonArray("SUBREGION_POLYGONS");
            
            for (int j = 0; j < subregionPolygons.size(); j++) {
                JsonArray pts = subregionPolygons.getJsonArray(j);
                ptCounter = 0;
                polygon = new Polygon();
                ArrPolygonPts = new Double[pts.size() * 2];
                
                for (int k = 0; k < pts.size(); k++) {
                    JsonObject pt = pts.getJsonObject(k);
                    Double x = getDataAsDouble(pt, "X");
                    Double y = getDataAsDouble(pt, "Y");
                    ArrPolygonPts[ptCounter] = (((x+180)/360)*width);
                    ptCounter += 1;
                    ArrPolygonPts[ptCounter] = (((y+90)/180)*height);
                    ptCounter += 1; 
                }
                polygon.getPoints().addAll(ArrPolygonPts);
                dataManager.addPolygon(polygon);
            }
        }
    }
    
    public double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    public int getDataAsInt(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber)value;
        return number.bigIntegerValue().intValue();
    }
    
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    public Polygon loadPolygon(JsonObject jsonObject) {
	// GET THE DATA
        double x = getDataAsDouble(jsonObject, "X");
        double y = getDataAsDouble(jsonObject, "Y");
        
	// THEN USE THE DATA TO BUILD AN ITEM
        Polygon poly = new Polygon(x,y);
        
	// ALL DONE, RETURN IT
	return poly;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
